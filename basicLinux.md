## Cheat Sheet #

**ls** Lists all files and directories in the present working directory <br />
**ls - R** Lists files in sub-directories as well <br />
**ls - a** Lists hidden files as well <br />
**ls - al** Lists files and directories with detailed information like permissions, size, owner, etc. <br />
**cat > filename** Creates a new file <br />
**cat filename** Displays the file content <br />
**cat file file2 > file3** Joins two files (file1, file2) and stores the output in a new file (file3) <br />
**mv  file "new file path"** Moves the files to the new location <br />
**mv filename new_file_name** Renames the file to a new filename <br />
**sudo** Allows regular users to run programs with the security privileges of the superuser or root <br />
**rm filename** Deletes a file <br />
**man** Gives help information on a command <br />
**history** Gives a list of all past commands typed in the current terminal session <br />
**clear** Clears the terminal <br />
**mkdir directoryname** Creates a new directory in the present working directory or a at the specified path <br />
**rmdir** Deletes a directory <br />
**mv** Renames a directory <br />
**pr -x** Divides the file into x columns <br />
**pr -h** Assigns a header to the file <br />
**pr -n** Denotes the file with Line Numbers <br />
**lp -nc lpr c** Prints "c" copies of the File <br />
**lp -d lp -P**	Specifies name of the printer <br />
**apt-get** Command used to install and update packages <br />
**mail -s 'subject' -c 'cc-address' -b 'bcc-address' 'to-address'** Command to send email <br />
**mail -s 'Subject' to-address > Filename** Command to send email with attachment <br />


## Permissions #

The characters are pretty easy to remember. 
- **"r"** read permission
- **"w"** write permission
- **"x"** = execute permission
- **"="** no permission

### Changing file/directory permissions with 'chmod' command #

**ls - l** on terminal gives <br />
![ALT](permissionsSystem.png) <br />

![ALT](permissions(1).png) <br />

We can use the 'chmod' command which stands for 'change mode'. Using the command, we can set permissions (read, write, execute) on a file/directory for the owner, group and the world. Syntax:
```
chmod permissions filename
```

There are 2 ways to use the command -

**1. Absolute mode** <br />
The table below gives numbers for all for permissions types: <br />
![ALT](permissionsTable.png) <br />

Let's see the chmod command in action. <br />

![ALT](chmod_new(1).png) <br />

![ALT](filePermissions(1).png) <br />

'764' absolute code says the following:
- Owner can read, write and execute
- Usergroup can read and write
- World can only read

**2. Symbolic mode**
In the Absolute mode, you change permissions for all 3 owners. In the symbolic mode, you can modify permissions of a specific owner. It makes use of mathematical symbols to modify the file permissions.

- **'+'**	Adds a permission to a file or directory
- **'-'**	Removes the permission
- **'='**	Sets the permission and overrides the permissions set earlier

The various owners are represented as:
- **u**	user/owner
- **g**	group
- **o**	other
- **a**	all

![ALT](symbolic_Mode(1).png)


## Pipe, Grep and Sort Command in Linux/Unix #

### What is a Pipe in Linux? #
The Pipe is a command in Linux that lets you use two or more commands such that output of one command serves as input to the next. In short, the output of each process directly as input to the next one like a pipeline. The symbol '|' denotes a pipe. <br />
Pipes help you mash-up two or more commands at the same time and run them consecutively. <br />
Let us understand this with an example. <br />
When you use 'cat' command to view a file which spans multiple pages, the prompt quickly jumps to the last page of the file, and you do not see the content in the middle. <br />
To avoid this, you can pipe the output of the 'cat' command to 'less' which will show you only one scroll length of content at a time.
```
cat filename | less
```
An illustration would make it clear. <br />
![ALT](Piping.png)

**1. Commandes 'pg' et 'more'** <br />
Instead of 'less', you can also use.
```
cat Filename | pg 
```
or
```
cat Filename | more
```

![ALT](more.png)

**2. The 'grep' command** <br />
Suppose you want to search a particular information the postal code from a text file. <br />
You may manually skim the content yourself to trace the information. A better option is to use the grep command. It will scan the document for the desired information and present the result in a format you want.
```
cat Filename | grep search_string
```
Following options can be used with this command:
**-v**	Shows all the lines that do not match the searched string
**-c**	Displays only the count of matching lines
**-n**	Shows the matching line and its number
**-i**	Match both (upper and lower) case
**-l**	Shows just the name of the file with the string

**3. The 'sort' command** <br />
This command helps in sorting out the contents of a file alphabetically.
```
sort Filename
```
There are extensions to this command as well, and they are listed below.
**-r**  Reverses  sorting
**-n**  Sorts numerically
**-f**  Case insensitive sorting

**3. What is a Filter?** <br />
Linux has a lot of filter commands like awk, grep, sed, spell, and wc. A filter takes input from one command, does some processing, and gives output. <br />
When you pipe two commands, the "filtered " output of the first command is given to the next. <br />
Example:We want to highlight only the lines that do not contain the character 'a', but the result should be in reverse order. <br />
For this, the following syntax can be used.
```
cat Filename | grep -v a | sort - r
```

## Linux Regular Expression Tutorial: Grep Regex Example #

**What are Regular Expressions?** <br />
Regular expressions are special characters which help search data, matching complex patterns. Regular expressions are shortened as 'regexp' or 'regex'.

### Basic Regular expressions # 
Some of the commonly used commands with Regular expressions are tr, sed, vi and grep. Listed below are some of the basic Regex.

- **"."**	replaces any character
- **"^"**	matches start of string
- **"$"**	matches end of string
- **"*"**	matches up zero or more times the preceding character
- **"\"**	Represent special characters
- **"()"**	Groups regular expressions
- **"?"**	Matches up exactly one character

### Interval Regular expressions #
These expressions tell us about the number of occurrences of a character in a string. They are :
**{n}**	    Matches the preceding character appearing 'n' times exactly
**{n,m}** 	Matches the preceding character appearing 'n' times but not more than m
**{n, }**	Matches the preceding character only when it appears 'n' times or more

**Example:** <br />
Filter out all lines that contain character 'p' <br />
![ALT](regex6.png) <br />

We want to check that the character 'p' appears exactly 2 times in a string one after the other. For this the syntax would be:
```
cat sample | grep -E p\{2}
```

![ALT](regex7.png) <br />

Note: You need to add -E with these regular expressions.

### Extended regular expressions #
These regular expressions contain combinations of more than one expression. Some of them are:
- **\+** Matches one or more occurrence of the previous character
- **\?** Matches zero or one occurrence of the previous character

**Example:** <br />
Searching for all characters 't' <br />
![ALT](regex8.png) <br />

Suppose we want to filter out lines where character 'a' precedes character 't' <br />
We can use command like
```
cat sample|grep "a\+t"
```

![ALT](regex9.png)

### Brace expansion #
The syntax for brace expansion is either a sequence or a comma separated list of items inside curly braces "{}". The starting and ending items in a sequence are separated by two periods "..".

**Some examples:** <br />

![ALT](brace_expansion.png) <br />

In the above examples, the echo command creates strings using the brace expansion.


## List of Environment Variables in Linux/Unix #

### What is a Computing Environment? #
The Computing environment is the Platform(Platform = Operating System+ Processor) where a user can run programs.

### What is a Variable? #
In computer science, a variable is a location for storing a value which can be a filename, text, number or any other data. It is usually referred to with its Symbolic name which is given to it while creation. The value thus stored can be displayed, deleted, edited and re-saved. <br />
Variables play an important role in computer programming because they enable programmers to write flexible programs. As they are related to the Operating system that we work on, it is important to know some of them and how we can influence them.

### What are Environment variables? #
Environment variables are dynamic values which affect the processes or programs on a computer. They exist in every operating system, but types may vary. Environment variables can be created, edited, saved, and deleted and give information about the system behavior. <br />
Environment variables can change the way a software/programs behave. <br />
E.g. $LANG environment variable stores the value of the language that the user understands. This value is read by an application such that a Chinese user is shown a Mandarin interface while an American user is shown an English interface. <br />
Let's study some common environment variables -
- **PATH**	This variable contains a colon (:)-separated list of directories in which your system looks for executable files. When you enter a command on terminal, the shell looks for the command in different directories mentioned in the $PATH variable. If the command is found, it executes. Otherwise, it returns with an error 'command not found'.
- **USER**	   The username
- **HOME**	   Default path to the user's home directory
- **EDITOR**   Path to the program which edits the content of files
- **UID**	   User's unique ID
- **TERM**	   Default terminal emulator
- **SHELL**	   Shell being used by the user

### Accessing variables values, set new environnement variables, deleting variables #

- **echo $VARIABLE**	              To display value of a variable
- **env**	                          Displays all environment variables
- **VARIABLE_NAME= variable_value**	  Create a new variable <br />
![ALT](new_variable.png) <br />

- **unset**  	                      Remove a variable <br />
![ALT](unset.png) <br />

- **export Variable=value**	          To set value of an environment variable


## Linux/Unix SSH, Ping, FTP, Telnet Communication Commands #

While working on a Linux operating system, you may need to communicate with other devices. For this, there are some basic utilities that you can make use of. <br />
These utilities can help you communicate with:
- **networks**
- **other Linux systems**
- **and remote users**
So, let us learn them one by one.
- **SSH**
- **Ping**
- **FTP**
- **Telnet**

### SSH #
SSH which stands for Secure Shell, It is used to connect to a remote computer securely. Compare to Telnet, SSH is secure wherein the client /server connection is authenticated using a digital certificate and passwords are encrypted. Hence it's widely used by system administrators to control remote Linux servers. <br />
The syntax to log into a remote Linux machine using SSH is
```
SSH username@ip-address or hostname
```

### Ping #
This utility is commonly used to check whether your connection to the server is healthy or not.This command is also used in:
- Analyzing network and host connections
- Tracking network performance and managing it
- Testing hardware and software issues
Command Syntax:-
```
ping hostname="" or=""
```
Example: <br />
![ALT](ping_hostname.png) <br />

![ALT](ping.png) <br />
Here, A system has sent 64 bytes data packets to the IP Address (172.16.170.1) or the Hostname(www.google.com). If even one of data packets does not return or is lost, it would suggest an error in the connection. Usually, internet connectivity is checked using this method.
You may Press Ctrl + c to exit from the ping loop.

### FTP #
FTP is file transfer protocol. It's the most preferred protocol for data transfer amongst computers.
You can use FTP to -
- Logging in and establishing a connection with a remote host
- Upload and download files
- Navigating through directories
- Browsing contents of the directories
The syntax to establish an FTP connection to a remote host is -
```
ftp hostname="" or=""
```
Once you enter this command, it will ask you for authentication via username and password. <br />
![ALT](FTP_login.png) <br />

Once a connection is established, and you are logged in, you may use the following commands to perform different actions.
- **dir**	            Display files in the current directory of a remote computer
- **cd "dirname"**	    Change directory to "dirname" on a remote computer
- **put file**	        Upload 'file' from local to remote computer
- **get file**	        Download 'file' from remote to local computer
- **quit**	            Logout

### Telnet #
Telnet helps to -
- connect to a remote Linux computer
- run programs remotely and conduct administration
This utility is similar to the Remote Desktop feature found in Windows Machine. <br />
The syntax for this utility is:
```
telnet hostname="" or=""
Example:
telnet localhost
```
For demonstration purpose, we will connect to your computer (localhost). The utility will ask your username and password. <br />
![ALT](telnet_localhost.png) <br />

Once authenticated, you can execute commands just like you have done so far, using the Terminal. The only difference is, if you are connected to a remote host, the commands will be executed on the remote machine, and not your local machine. <br />
You may exit the telnet connection by entering the command 'logout'.


## Linux/Unix Process Management: ps, kill, top, df, free, nice Commands #

Any running program or a command given to a Linux system is called a process. <br />
A process could run in foreground or background. <br />
The priority index of a process is called Nice in Linux. Its default value is 0, and it can vary between 20 to -19. <br />
The lower the Niceness index, the higher would be priority given to that task. <br />

### 1. bg #	    
To send a process to the background. <br />
![ALT](bg.jpg)

### 2. fg #	    
To run a stopped process in the foreground.
```
fg jobname
```
![ALT](fg.png)

### 3. top #
Details on all Active Processes. <br />
![ALT](top.png) <br />

The terminology follows:
- **PID**	    The process ID of each task	(1525,961...)
- **User**	    The username of task owner	(Home,Root...)
- **PR**	    Priority Can be 20(highest) or -20(lowest) (20,20...)
- **NI**	    The nice value of a task (0,0...)
- **VIRT**	    Virtual memory used (kb) (1775,75972...)
- **RES**	    Physical memory used (kb) (100,51...)
- **SHR**	    Shared memory used (kb) (28,7952...)
- **S**	        Status
There are five types: <br />
'D' = uninterruptible sleep <br />
'R' = running <br />
'S' = sleeping <br />
'T' = traced or stopped <br />
'Z' = zombie <br />
- **%CPU**	    % of CPU time (1.7,1.0...)
- **%MEM**	    Physical memory used (10,5.1...)
- **TIME+**	    Total CPU time (5:05.34,2:23.42...)
- **Command**	Command name (Photoshop.exe,Xorg...)

### 3. ps #	    
Gives the status of processes running for a user.
```
ps ux
```
![ALT](ps.png) <br />

### 4. ps PID #	
Gives the status of a particular process. <br />
You can also check the process status of a single process, use the syntax -
```
ps PID 
```

![ALT](ps_pid.jpg)

### 5. pidof #
Gives the Process ID (PID) of a process. <br />

![ALT](kill.png)

### 6. kill PID #	
Kills a process
```
kill PID
```

### 7. nice #
Starts a process with a given priority.
```
nice -n 'Nice value' process name
```

![ALT](changing_niceness.png)

### 8. renice #	
Changes priority of an already running process.
```
renice 'nice value' -p 'PID'
```

![ALT](renicing.png)

### 9. df #
Gives free hard disk space on your system.
```
'df -h' 
```

![ALT](df-h.png)

### 10. free #
Gives free RAM on your system. You can use the arguments:
- **free -m** to display output in MB
- **free -g** to display output in GB


## Shell Scripting #

### What is a Shell? #
An Operating is made of many components, but its two prime components are -
- Kernel
- Shell <br />
![ALT](ShellScripting.png) <br />

A Kernel is at the nucleus of a computer. It makes the communication between the hardware and software possible. While the Kernel is the innermost part of an operating system, a shell is the outermost one. <br />
A shell in a Linux operating system takes input from you in the form of commands, processes it, and then gives an output. It is the interface through which a user works on the programs, commands, and scripts. A shell is accessed by a terminal which runs it. <br />
When you run the terminal, the Shell issues a command prompt (usually $), where you can type your input, which is then executed when you hit the Enter key. The output or the result is thereafter displayed on the terminal. <br />
The Shell wraps around the delicate interior of an Operating system protecting it from accidental damage. Hence the name Shell. <br />

- The Bourne shell and the C shell are the most used shells in Linux.
- Shell scripting is writing a series of command for the shell to execute.
- Shell variables store the value of a string or a number for the shell to read.
- Shell scripting can help you create complex programs containing conditional statements, loops, and functions.

### What is Shell Scripting? #
Shell scripting is writing a series of command for the shell to execute. It can combine lengthy and repetitive sequences of commands into a single and simple script, which can be stored and executed anytime. This reduces the effort required by the end user. <br />
Let us understand the steps in creating a Shell Script:
- Create a file using a vi editor(or any other editor).  Name  script file with extension .sh
- Start the script with #! /bin/sh
- Write some code.
- Save the script file as filename.sh
- For executing the script type bash filename.sh
- "#!" is an operator called shebang which directs the script to the interpreter location. So, if we use"#! /bin/sh" the script gets directed to the bourne-shell. <br />
Let's create a small script -
```
#!/bin/sh
ls
```
Let's see the steps to create it - <br />
![ALT](vi_scriptsample.png)
Command 'ls' is executed when we execute the scrip sample.sh file.

### Adding shell comments #
Commenting is important in any program. In Shell programming, the syntax to add a comment is
```
#comment
```

### What are Shell Variables? #
As discussed earlier, Variables store data in the form of characters and numbers. Similarly, Shell variables are used to store information and they can by the shell only.  <br />
For example, the following creates a shell variable and then prints it: <br />
![ALT](program.jpg)


## Linux/Unix Virtual Terminal #

Linux is a multi-user system, which allows many users to work on it simultaneously. So what if different users need to work on the same system at a time? How do you do that?
- Virtual terminals are CLIs which execute the user commands
- There are six virtual terminals which can be launched using the shortcut keys
- They offer multi-user environment, and up to six users can work on them at the same time
- Unlike terminals, you cannot use mouse with virtual terminals
- To launch a virtual terminal press Ctrl+Alt+F(1 to 6) on the keyboard
- Use the same command for navigating through the different terminals
tty is the teletype number which you can also know by typing the command "tty". <br />
![ALT](tty.png)

- To return to the home screen of the Linux system, use Ctrl+Alt+F7 and it would take to you the terminal

### Virtual Terminal shortcuts #
These are some of the shortcuts that you should be aware of while working on virtual terminals.
- **Home or Ctrl + a**	Move the cursor to the start of the current line
- **End or Ctrl + e**	Move the cursor to the end of the current line
- **Tab**	            Autocomplete commands
- **Ctrl + u**	        Erase the current line
- **Ctrl + w**	        Delete the word before the cursor
- **Ctrl + k**      	Delete the line from the cursor position to the end
- **reset**	            Reset the terminal
- **history**	        List of commands executed by the user
- **Arrow up**      	Scroll up in history and enter to execute
- **Arrow down**    	Scroll down in history and enter to execute
- **Ctrl + d**      	Logout from the terminal
- **Ctrl + Alt + Del**	Reboot the system


## User Administration: adduser, usermod, userdel #

As Linux is a multi-user operating system, there is a high need of an administrator, who can manage user accounts, their rights, and the overall system security. <br />
1. You can use both GUI or Terminal for User Administration.
2. You can create, disable and remove user accounts.
3. You can add/delete a user to a usergroup.
Here is a list of linux user management commands:
- **Add a user**
```
sudo adduser username
```
![ALT](sudo_adduser.png)

- **Disable a user**
```
sudo passwd -l 'username'
```

- **Delete a user**
```
sudo userdel -r 'username'
```

- **View the existing groups**
```
groups
```
- **Add user a to a usergroup**
```
sudo usermod -a -G GROUPNAME USERNAME
```

- **Remove user from a user group**
```
sudo deluser USER GROUPNAME
```

- **Give information on all logged in user**
```
finger
```

- **Gives information of a particular user**
```
finger username
```                           